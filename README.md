# Gitlab-CI | Mise en place d'un pipeline CI : build d'une image docker - production d'un artifact - test d'acceptance

<p align="center">
Please find the specifications by clicking
  <a href="https://github.com/eazytrainingfr/alpinehelloworld.git" alt="Crédit : eazytraining.fr" >
  </a>
</p>
------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


<img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 

LinkedIn : https://www.linkedin.com/in/carlinfongang/

## l'objectif de ce labs
Mettre en place une pipeline-ci sur Gitlab-ci, qui va parmettre de builder une image docker embarquant le code de l'application à déployer, de produire un artefact qui sera utilisé pour la phase de test d'acceptance dans un nouveau job de test, qui sera rajouter à ce pipeline et exécuté après le build de l'image docker.

## reaction du .gitlab-ci.yml
structurer le script en plusieurs stages en s'assurant que le build s'exécute avant le test
>![Alt text](img/image.png)

## script de génération et sauvegarde de l'artifact
>![Alt text](img/image-1.png)

## usage de l'artifact/image docker buildé dans le lancement d'un conteur pour test d'acceptation
>![Alt text](img/image-2.png)

## exécution du pipele : tigger : push
### exécution du job 1 : build de l'image docker, création de l'artifact et sauvegarde de l'artifact
il est important de noter que le rennur utilisé ici, est un rennur privé mis en place sur un serveur local, il faudra changer les tag dans le script ou les supprimer pour utiliser les runner par défaut proposé par gitlab CI/CD
>![Alt text](img/image-3.png)

### exécution du job 2 : lancement d'un conteneur avec l'artifacte sauvegardé au job 1
le temps d'exécution long s'explique par le fait de l'utilisation de runner privé en local qui nécessite un certain temps pour le pull et push des images, artifact et le run des machines
>![Alt text](img/image-4.png)
>![Alt text](img/image-5.png)
>![Alt text](img/image-6.png)

### checking du déroulement du run du conteneur avec l'artifact
on peut constater que le run se déroule bien à partir de la lecture de la ligne 49
>![Alt text](img/image-9.png)

### checking de l'artifact .tar produit et save
>![Alt text](img/image-7.png)
>![Alt text](img/image-8.png)

### chargement de l'artifact/décompression pour l'usage dans le conteneur de test
>![Alt text](img/image-9.png)

### Test d'acceptance validé, 
la requette `curl "http://docker-vm" | grep -q "Hello world!"` fontionne
>![Alt text](img/image-10.png)